class CreateDemandaCategorias < ActiveRecord::Migration
  def change
    create_table :demanda_categorias do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
