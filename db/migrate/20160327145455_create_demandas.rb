class CreateDemandas < ActiveRecord::Migration
  def change
    create_table :demandas do |t|
      t.string :latitude
      t.string :longitude
      t.text :descricao

      t.timestamps null: false
    end
  end
end
