class AddBiografiaToUser < ActiveRecord::Migration
  def change
    add_column :users, :biografia, :text
  end
end
