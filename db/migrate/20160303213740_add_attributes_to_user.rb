class AddAttributesToUser < ActiveRecord::Migration
  def change
    add_column :users, :username, :string
    add_column :users, :name, :string
    add_column :users, :candidato, :bool
    add_column :users, :partido_id, :integer
  end
end
