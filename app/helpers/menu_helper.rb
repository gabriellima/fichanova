module MenuHelper
  def active_menu(controller_name)
    controller.controller_name == controller_name.to_s ? 'active' : ''
  end
end
