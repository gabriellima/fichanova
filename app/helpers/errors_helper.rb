module ErrorsHelper

  def specific_error_for(field_name, options)
    resource = options[:resource]
    if resource.errors[field_name].any?
      content_tag(:div, class: 'specific-error') do
        resource.errors.full_messages_for(field_name).each do |error|
          concat(
            content_tag(:span, class: 'error') do
              error
            end
          )
        end
      end
    end
  end

end
