initializeComponent = ->
  map = new google.maps.Map $("#mapa")[0], {
    center: {lat: -15.888697, lng: -47.812500},
    zoom: 8,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  }

  input = $("#pac-input")[0]
  searchBox = new google.maps.places.SearchBox input
  map.controls[google.maps.ControlPosition.TOP_LEFT].push input

  map.addListener 'bounds_changed', ->
    searchBox.setBounds map.getBounds()

  markers = []
  searchBox.addListener 'places_changed', ->
    places = searchBox.getPlaces()
    return if places.length == 0

    markers.forEach (marker) ->
      marker.setMap null
    markers = []

    bounds = new google.maps.LatLngBounds()
    places.forEach (place) ->
      $('#demanda_latitude').val place.geometry.location.lat()
      $('#demanda_longitude').val place.geometry.location.lng()

      icon = {
        url: place.icon,
        size: new google.maps.Size(71, 71),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(17, 34),
        scaledSize: new google.maps.Size(25, 25)
      }

      markers.push new google.maps.Marker {
        map: map,
        icon: icon,
        title: place.name,
        position: place.geometry.location
      }

      if place.geometry.viewport
        bounds.union place.geometry.viewport
      else
        bounds.extend place.geometry.location

    map.fitBounds bounds

inicializaMapaLocalizacaoDemanda = ->
  latitude = parseFloat( $("#latitude").val() )
  longitude = parseFloat( $("#longitude").val() )

  if latitude and longitude
    latLng = {lat: latitude, lng: longitude}

    mapaDemanda = new google.maps.Map document.getElementById("mapa-demanda"), {
      zoom: 16,
      center: latLng
    }

    marker = new google.maps.Marker {
      position: latLng,
      map: mapaDemanda
    }

google.maps.event.addDomListener window, "load", initializeComponent
google.maps.event.addDomListener window, "load", inicializaMapaLocalizacaoDemanda
