$ ->
  if not $('#user_candidato').prop 'checked'
    $('.select-partido').hide()

  $('#user_candidato').click ->
    element = $(this)
    checkPartido = $('.select-partido')
    if element.prop 'checked'
      checkPartido.show()
    else
      checkPartido.hide()

  if $('#city_data').val()
    data = {}
    data.id = $('#city_data').val().split('|')[0]
    data.text = $('#city_data').val().split('|')[1]

    $('#user_city_id').append(
      "<option value='#{data.id}'>#{data.text}</option>"
    ).trigger 'change'

  $('#user_city_id').select2({
    theme: "bootstrap"
    placeholder: "Selecione uma cidade"
    allowClear: true
    ajax: {
      url: '/cidades/list_cidade_estado',
      dataType: 'json',
      delay: 250,
      data: (params) ->
        return {
          q: params.term,
          page: params.page
        }
      processResults: (data, params) ->
        $('#user_city_id').find('option').remove()
        return {
          results: $.map data, (item) ->
            return {
              id: item.id,
              text: "#{item.name} - #{item.state_name}"
            }
        }
    }
  })
