class CidadesController < ApplicationController
  layout false

  def list_cidade_estado
    @cidades = City.cidade_estado params[:q]
    render json: @cidades
  end

end
