class DemandasController < ApplicationController
  before_action :authenticate_user!

  def index
    @demandas = Demanda.all
  end

  def new
    @demanda = Demanda.new
  end

  def create
    @demanda = Demanda.new demanda_params
    @demanda.user = current_user
    respond_to do |format|
      if @demanda.save
        format.js {render js: "window.location = '#{demandas_path}'"}
      else
        format.js
      end
    end
  end

  def show
    @demanda = Demanda.find params[:id]
    @comments = @demanda.comments
  end

  private
    def demanda_params
      params.require(:demanda).permit :latitude, :longitude, :titulo, :descricao,
                                      :demanda_categoria_id
    end

end
