class ProfilesController < ApplicationController
  before_action :authenticate_user!

  def edit
    @user = current_user
  end

  def update
    @user = current_user
    if @user.update user_params
      if @user.candidato?
        redirect_to biografia_candidato_path(@user)
      else
        redirect_to eleitor_candidato_path(@user)
      end
    else
      render :edit
    end
  end

  def edit_password
    @user = current_user
  end

  def update_password
    @user = current_user
    if @user.update_with_password password_params
      sign_in @user, bypass: true
      redirect_to root_path
    else
      render :edit_password
    end
  end

  private
  def user_params
    permitted_params = params.require(:user)
                             .permit(:email, :username, :name, :candidato,
                                     :partido_id, :biografia, :picture, :city_id)

    # Gem select2 doesn't send the city_id when none is selectd
    permitted_params.merge!(city_id: '') if params[:user][:city_id].nil?
    permitted_params
  end

  def password_params
    params.require(:user).permit :password, :password_confirmation, :current_password
  end

end
