class CandidatosController < ApplicationController
  before_action :authenticate_user!
  after_action :redirect_unless_candidato, except: [:index]

  def index
    @candidatos = User.candidatos
  end

  def biografia
    @user = User.find params[:id]
  end

  def propostas
    @user = User.find params[:id]
  end

  private
  def redirect_unless_candidato
    unless @user.candidato?
      redirect_to root_path
    end
  end

end
