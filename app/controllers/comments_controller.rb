class CommentsController < ApplicationController
  before_action :authenticate_user!

  def create
    @resource = if params[:demanda_id].present?
      Demanda.find params[:demanda_id]
    end

    @comment = @resource.comments.build comment_params
    @comment.user = current_user
    @comment.save

    @comments = @resource.comments
    @demanda  = @resource

    render "create.js.erb"
  end

  private
    def comment_params
      params.require(:comment).permit :content
    end
end
