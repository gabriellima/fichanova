class City < ActiveRecord::Base
  belongs_to :state

  def self.cidade_estado(q)
    City.joins(:state)
      .select("cities.id, cities.name, states.name as state_name")
      .where("cities.name like ?", "%#{q}%")
      .limit(10)
  end
end
