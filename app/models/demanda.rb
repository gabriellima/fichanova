class Demanda < ActiveRecord::Base
  belongs_to :user
  belongs_to :demanda_categoria

  has_many :comments, as: :commentable

  validates_presence_of :titulo, :descricao, :demanda_categoria_id
  validate :map_address_selected?

  def map_address_selected?
    unless latitude.present? || longitude.present?
      errors.add :endereco, "can't be blank"
    end
  end
end
