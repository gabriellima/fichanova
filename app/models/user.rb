class User < ActiveRecord::Base
  belongs_to :city
  has_many :demandas, dependent: :destroy
  has_many :comments, dependent: :destroy
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  # Paperclip configuration
  has_attached_file :picture, styles: {medium: "300x300", thumb: "100x100"},
    default_url: "/images/missing.png"

  validates_attachment_content_type :picture, content_type: /\Aimage\/.*\Z/

  validates :partido_id, presence: true, if: :candidato?
  validates :username, presence: true, uniqueness: true
  validates :name, presence: true, uniqueness: true

  validates_presence_of :city_id

  def primeiro_nome
    all_names.first
  end

  def ultimo_nome
    all_names.last
  end

  def all_names
    name.split
  end

  def self.candidatos
    where candidato: 1
  end

  def candidato?
    candidato.to_i == 1
  end

  def eleitor?
    !candidato?
  end
end
